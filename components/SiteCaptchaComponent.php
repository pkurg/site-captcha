<?php namespace Pkurg\SiteCaptcha\Components;

use Cms\Classes\ComponentBase;

class SiteCaptchaComponent extends ComponentBase
{

    public $captcha_key;
    public $captcha_img;

    public function componentDetails()
    {
        return [
            'name' => 'Site Captcha Component',
            'description' => 'Display a captcha on the page',
        ];
    }

    public function defineProperties()
    {
        return [
            'type' => [
                'title' => 'Captcha type',
                'type' => 'dropdown',
                'default' => 'default',
                'placeholder' => 'Select captcha type',
                'options' => [
                    'default' => 'default',
                    'math' => 'math',
                    'flat' => 'flat',
                    'mini' => 'mini',
                    'inverse' => 'inverse',

                ],
            ], 'showrefresh' => [
                'title' => 'Refresh button',
                'type' => 'dropdown',
                'default' => 'show',
                'options' => [
                    'show' => 'show',
                    'hide' => 'hide',
                ],
            ],
            'iconclass' => [
                'title' => 'Icon class',
                'default' => 'icon-refresh',
                'type' => 'string',
            ],

        ];
    }

    public function onRun()
    {

        $captcha = \Captcha::create($this->property('type'), true);
        $this->captcha_key = isset($captcha['key']) ? $captcha['key'] : null;
        $this->captcha_img = isset($captcha['img']) ? $captcha['img'] : null;
        $this->addJs('/plugins/pkurg/sitecaptcha/assets/captcha.js?v=7');

    }

}
